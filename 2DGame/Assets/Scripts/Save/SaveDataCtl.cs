﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;                    //裡面將資料轉成文字檔的Function
using System.Xml.Serialization;     //序列化的指令

public class SaveDataCtl : MonoBehaviour
{
    public void SaveData ( PlayerData obj )
    {
        Debug.Log ("Save!");
        //將資料轉成xml檔
        XmlSerializer serializer = new XmlSerializer ( typeof ( PlayerData ) );
        //將xml檔存入至電腦中
        StreamWriter writer = new StreamWriter ( "PlayerData.xml" );

        serializer.Serialize ( writer.BaseStream, obj );
        //關閉
        writer.Close ();
    }

    public PlayerData LoadData ()
    {
        Debug.Log ("Load!");
        //將資料轉成xml檔
        XmlSerializer serializer = new XmlSerializer ( typeof ( PlayerData ) );
        //從路徑中讀取xml檔
        StreamReader reader = new StreamReader ( "PlayerData.xml" );
        //將讀取到的xml檔轉換成PlayerData
        PlayerData deserialized = (PlayerData) serializer.Deserialize ( reader.BaseStream );
        //關閉
        reader.Close ();
        return deserialized;
    }
}
