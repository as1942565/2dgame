﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerDataTemplate : MonoBehaviour
{
    public PlayerData data;

    private void Start ()
    {
        GetComponent<SaveDataCtl> ().SaveData ( data );
    }
}
