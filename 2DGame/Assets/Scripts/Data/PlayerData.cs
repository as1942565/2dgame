﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

[Serializable]
public class PlayerData
{
    public int HP;                  //血量
    public int LifeNum;             //生命數
    public int CoinNum;             //金幣數量
    public int Score;               //分數
    public bool Power;              //力量
    public int Level;               //關卡總數

    public bool [] LoseHP;          //是否有遺失血量

    public string LevelNum ( int level ) //關卡幾之幾
    {
        string LevelStr = ( level / 8 + 1 ).ToString() + "-" + ( level % 8 ).ToString ();
        return LevelStr;
    }
}
