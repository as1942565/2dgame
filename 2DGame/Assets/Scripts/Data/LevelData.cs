﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LevelData : MonoBehaviour
{
    public Vector2 CamXRange;       //每個關卡的起點與終點的X軸範圍
    public int LevelTime;           //每個關卡的剩餘時間

    public static LevelData self;   //建構式
    private void Awake ()
    {
        self = this;
    }
}
