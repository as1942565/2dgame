﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerDataManager : MonoBehaviour
{
    public static PlayerDataManager self;
    private void Awake ()
    {
        self = this;
        data = new PlayerData ();
        Load ();
    }

    public PlayerData data;

    public void Save ()             //存檔
    {
        GetComponent<SaveDataCtl> ().SaveData ( data );
    }

    public void Load ()             //讀檔
    {
        data = GetComponent<SaveDataCtl> ().LoadData ();
    }
}
