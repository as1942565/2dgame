﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PauseCtl : MonoBehaviour
{
    public bool PauseBool;              //遊戲是否暫停
    public float TrueTime;              //遊戲真正運行的時間 (暫停時就會停止計算

    public static PauseCtl self;        //建構式
    private void Awake ()
    {
        self = this;
    }

    private void Update ()
    {
        if ( PauseBool == false )       //如果現在遊戲不是暫停的狀態
            TrueTime += Time.deltaTime; //計算真正運行的時間

    }

    public void Pause ( bool pause )    //切換遊戲 暫停 / 開始
    {
        PauseBool = pause;              //切換遊戲 暫停 / 開始
        //顯示暫停的UI
    }

    public void ResetTime ()            //遊戲真正運行的時間 與 關卡剩餘時間
    {
        TrueTime = 0;
        TimeCtl.self.TimeCount = LevelData.self.LevelTime;
    }
}
