﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ScoreCtl : MonoBehaviour
{
    public static ScoreCtl self;
    private void Awake ()
    {
        self = this;
    }

    public Text ScoreText;

    private int ScoreNum = 0;
    //const : 此變數將不可再更改
    private const string zero = "00000";            //總分的總位數

    private void Start ()
    {
        AddScore ( PlayerDataManager.self.data.Score );
    }

    public void AddScore ( int score )              //得分
    {
        ScoreNum += score;                          //將得到的分數加進總分內
        if ( ScoreNum <= 99999 )                    //如果總分小於等於99999
        { 
            string ScoreStr = ScoreNum.ToString (); //先將總分轉成字串
            //將不足的位數以0來取代
            string TrueScore = zero.Substring ( 0 , zero.Length - ScoreStr.Length );
            ScoreText.text = TrueScore + ScoreStr;  //顯示分數
        }
        else                                        //否則 ( 也就是總分大於99999 )
        {
            ScoreText.text = "99999";               //一律顯示99999
        }
        PlayerDataManager.self.data.Score = ScoreNum;
    }
 
}
