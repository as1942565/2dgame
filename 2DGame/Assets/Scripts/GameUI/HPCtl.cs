﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HPCtl : MonoBehaviour
{
    public static HPCtl self;
    private void Awake ()
    {
        self = this;
    }

    private List<Image> HP = new List<Image>();                 //血量

    private float Distance = 75f;                               //每個血量的間隔
    private const string HPPrefabPath = "Prefabs/GameUI/HP";    //血量的Prefab路徑

    private void Start ()  
    {
        for ( int i = 0; i < PlayerDataManager.self.data.HP; i++ )          //依照需要的數量開始生產血量
        {
            GameObject hp = Instantiate ( Resources.Load ( HPPrefabPath ) , transform ) as GameObject;
            hp.transform.localPosition += new Vector3 ( Distance * i , 0 , 0 );
            hp.name = "HP-" + i.ToString ();
            hp.GetComponent<Image> ().enabled = !PlayerDataManager.self.data.LoseHP [i];
            HP.Add ( hp.GetComponent<Image>() );
        }
    }

    public void LoseHP ()                                       //遺失血量
    {
        for ( int i = HP.Count - 1; i >= 0 ; i-- )
        {
            if ( HP[i].enabled )
            {
                HP [i].GetComponent<Animation> ().Play ( "LoseLife" );
                PlayerDataManager.self.data.LoseHP [i] = true;
                return;
            }
        }
    }
}
