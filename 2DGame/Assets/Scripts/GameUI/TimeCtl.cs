﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TimeCtl : MonoBehaviour
{
    public int TimeCount;                  //剩餘時間

    public static TimeCtl self;
    private void Awake ()
    {
        self = this;
    }
    public Text TimeText;                   //顯示剩餘的時間的文字

    private const string zero = "000";      //時間的總位數

    private void Update ()
    {
        //如果現在是暫停的狀態
        if ( PauseCtl.self.PauseBool == true )
        {
            return;                         //就回傳
        }

        //將剩餘時間轉成字串
        string TimeStr = ( TimeCount - (int)PauseCtl.self.TrueTime ).ToString ();

        if ( int.Parse ( TimeStr ) >= 0 )
        {
            //將不足的位數以0來取代
            string TrueTime = zero.Substring ( 0, zero.Length - TimeStr.Length );
            TimeText.text = TrueTime + TimeStr; //顯示剩餘時間
        }
        else
        {
            Debug.Log ("TimesUp!!");
        }
    }
}
