﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GetCoinCtl : MonoBehaviour
{
    public static GetCoinCtl self;
    private void Awake ()
    {
        self = this;
    }

    public Text GetCoinText;            //金幣數的文字

    private int CoinNum = 0;            //金幣數

    private void Start ()
    {
        AddCoin ( PlayerDataManager.self.data.CoinNum );
    }

    public void AddCoin ( int coin )    //增加錢幣
    {
        CoinNum += coin;                //增加錢幣
        while ( CoinNum > 99 )          //如果錢幣大於99
        {
            CoinNum -= 100;             //就減100
            PlayerDataManager.self.data.LifeNum++;  //並且補一條命
        }

        if ( CoinNum < 0 )                 //如果金幣數小於0
        {
            CoinNum = 0;                   //就設定為0
        }

        /*if ( CoinNum > 9 )
            GetCoinText.text = CoinNum.ToString ();
        else
            GetCoinText.text = "0" + CoinNum.ToString ();*/
        //如果金幣數大於9 就將金幣數的文字設定為金幣數 否則在前面加個"0"
        GetCoinText.text = CoinNum > 9 ? "X " + CoinNum.ToString () : "X 0" + CoinNum.ToString ();
        PlayerDataManager.self.data.CoinNum = CoinNum;
    }
}
