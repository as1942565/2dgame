﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MainCamCtl : MonoBehaviour
{
    public static MainCamCtl self;
    private void Awake ()
    {
        self = this;
    }

    public Transform Player;

    private Vector2 MoveRangeX = new Vector2 ( -2 , 15 );

    private void Update ()      //執行無限次 ( 針對電腦效能 )
    {
        //將玩家的 相對位置 設定給 掛載此腳本 的 座標 的 相對位置
        transform.localPosition = Player.localPosition;             //(修正前)
        float CamX = Mathf.Clamp ( transform.localPosition.x , MoveRangeX.x , MoveRangeX.y );
        transform.localPosition = new Vector3 ( CamX , -3 , 0 );    //(修正後)
    }
}
