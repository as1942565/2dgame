﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MakerCamCtl : MonoBehaviour
{
    public static MakerCamCtl self;             //建構式
    private void Awake ()
    {
        self = this;
    }

    //攝影機X軸的移動範圍
    private Vector2 MoveRangeX = new Vector2 ( -2 , 15 );
    private float CamPosX = -2f;                 //攝影機的X軸位置

    public void CamMove ( float pos )            //每次移動編輯器後移動攝影機
    {
        //取得攝影機的X軸的範圍
        CamPosX = Mathf.Clamp ( pos , MoveRangeX.x , MoveRangeX.y );
        //更新攝影機位置
        transform.position = new Vector3 ( CamPosX , transform.position.y , transform.position.z );
    }
}
