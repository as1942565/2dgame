﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;          //使用額外擴充插件(DoTween)需要的指令

public class CamMoveCtl : MonoBehaviour
{
    public GameObject UI;

    private float Duration = 3f;

    private void Start ()
    {
        PauseCtl.self.ResetTime ();
        UI.SetActive ( false );
        PauseCtl.self.PauseBool = true;
        GetComponent<MainCamCtl> ().enabled = false;
        transform.localPosition = new Vector3 ( LevelData.self.CamXRange.y , -3 , 0 );
        Invoke ( "CamMove" , 1.5f );
    }

    void CamMove ()
    {
        transform.DOLocalMoveX ( LevelData.self.CamXRange.x , Duration );
        Invoke ( "Go" , Duration );
    }

    void Go ()
    {
        UI.SetActive ( true );
        PauseCtl.self.PauseBool = false;
        GetComponent<MainCamCtl> ().enabled = true;
    }
}
