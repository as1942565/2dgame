﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class LoadingSceneCtl : MonoBehaviour
{
    public static LoadingSceneCtl self;         //建構式
    private void Awake ()
    {
        self = this;
    }

    private string SceneName;                   //需要切換的場景名稱
    Animation anim;                             //簡易式動畫

    private void Start ()
    {
        anim = GetComponent<Animation> ();      //取得簡液式動畫
    }

    public void ChangeScene ( string name , float delay )           //切換場景 ( 場景名稱 , 幾秒後切換 )
    {
        SceneName = name;                       //指派場景名稱
        anim.Play ( "FadeOut" );                //播放淡出動畫
        Invoke ( "GoToScene" , delay );         //幾秒後切換場景
    }

    void GoToScene ()                           //切換場景
    {
        SceneManager.LoadScene ( SceneName );   //切換場景
    }
}
