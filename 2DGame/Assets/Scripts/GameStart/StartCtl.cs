﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StartCtl : MonoBehaviour
{

    private void Update ()
    {
        if ( Input.GetKeyDown ( KeyCode.Return ) )              //如果按下鍵盤的Enter鍵
        {
            PlayerDataManager.self.data.Level += 1;                         //關卡數加1
            LoadingSceneCtl.self.ChangeScene ( "Level" , 0.5f );//0.5秒後進訴關卡資訊場景
        }
    }
}
