﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NeverDestroy : MonoBehaviour
{
    public List<GameObject> NeverDestrouObj;

    public static bool OnceBool = false;        //是否做過一次

    private void Awake ()
    {
        if ( !OnceBool )                        //如果還沒做過一次
        { 
            for ( int i = 0; i < NeverDestrouObj.Count; i++ )
            {
                GameObject obj = Instantiate ( NeverDestrouObj[i] ) as GameObject;
                DontDestroyOnLoad ( obj );
            }
            OnceBool = true;
        }
    }
}
