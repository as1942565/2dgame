﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerCtl : MonoBehaviour
{
    public bool HurtBool = false;      //用途 : 判斷玩家是否受到傷害

    public static PlayerCtl self;
    private void Awake ()
    {
        self = this;
    }

    private float speed = 10f;          //用途 : 玩家速度
    private float JumpHight = 950f;     //用途 : 玩家跳躍高度
    private bool RunBool = false;       //用途 : 判斷玩家是否在跑步
    private bool JumpBool = false;      //用途 : 判斷玩家是否在跳躍
    private bool IsGround = false;      //用途 : 判斷玩家是否踩在地上
    private bool IsJump = false;        //用途 : 判斷是否按下了空白鍵

    Rigidbody2D body;                   //Rigidbody2D : 2D物件的物理運算
    Animator anim;                      //Animator : 動畫控制器

    private void Start ()               //開始時執行一次
    {
        //掛載這個腳本 的 遊戲物件 的 取得組件 中 Rigidbody2D 設定為 body
        body = GetComponent<Rigidbody2D> ();
        //掛載這個腳本 的 遊戲物件 的 取得組件 中 Animator 設定為 anim
        //anim = this.transform.GetChild ( 0 ).GetComponent<Animator> ();
        //transform.GetChild ( 0 ) : 取得自己以下的子物件 ( 第幾個 )
        anim = transform.GetChild ( 0 ).GetComponent<Animator> ();

        Physics2D.gravity = new Vector3 ( 0 , -40 , 0 );
    }

    private void Update ()
    {
        if ( Input.GetKeyDown ( KeyCode.P ) )
        {
            PlayerDataManager.self.data.Power = true;
            Debug.Log ( "GetPower!" );
        }

        if ( Input.GetKeyDown ( KeyCode.O ) )
        {
            PauseCtl.self.Pause ( !PauseCtl.self.PauseBool );
            SetAnimSpeed ( PauseCtl.self.PauseBool == true? 0 : 1 );
        }
    }

    private void FixedUpdate ()         //執行無限次 ( 固定的時間 -> 0.02秒執行一次 )
    {
        //如果玩家沒有受傷 而且 現在不是暫停的狀態
        if ( HurtBool == false && PauseCtl.self.PauseBool == false )
        {
            Move ();                        //專門控制移動的函式
            Jump ();                        //專門控制跳躍的函式
            SetAnimator ();                 //專門設定動畫控制器
        }
    }

    void Move ()                        //專門控制移動的函式
    {
        //Input.GetAxis : 回傳一個-1~1的浮點數
        //                當按下左鍵或A鍵時 會從0慢慢到-1
        //                當按下右鍵或D鍵時 會從0慢慢到1
        float h = Input.GetAxis ( "Horizontal" );

        //h不等於0就代表正在跑步
        /*if ( h != 0 )               //如果 h 不等於 0 的話
        {
            RunBool = true;         //代表正在跑步
        }
        else                        //否則
        {
            RunBool = false;        //代表沒有跑步
        }*/

        RunBool = h != 0;           //我們把 ( h 不等於 0 ) 的結果 ( true / false ) 設定給 RunBool

        //body.velocity : 2D物件的物理運算 的 速度
        //把一個二維數 ( h , 2D物件的物理運算 的 速度 的 Y軸 ) 設定給2D物件的物理運算 的 速度
        body.velocity = new Vector2 ( h * speed , body.velocity.y );

        //Mathf.Sign 只要小於0的數值 都會回傳 -1 否則回傳1 ( 會幫你判斷這個數值是不是負數 )
        float Turn = Mathf.Sign ( h );
        if ( h != 0 )
            transform.localScale = new Vector3 ( Turn , 1 , 1 );       //我們將 ( Turn , 1 , 1 ) 設定給 座標 的 相對大小
    }

    void Jump ()
    {
        //&& : 而且 -> 兩者以上成立才成立
        if ( Input.GetKey ( KeyCode.Space ) && !IsJump && IsGround )       //如果按下鍵盤的空白鍵 而且 還沒按下了空白鍵 而且 睬在地板上
        {
            //body.AddForce : 物理運算 的 受力
            //Vector2.up : ( 0 , 1 , 0 )
            body.AddForce ( Vector2.up * JumpHight );               //讓玩家向上移動
        }
        IsJump = Input.GetKey ( KeyCode.Space );

        JumpBool = body.velocity.y > 0;
        IsGround = body.velocity.y == 0;
    }

    void SetAnimator ()                 //專門設定動畫控制器
    {
        //代表是否需要跑步
        anim.SetBool ( "Run" , RunBool );  //anim.SetBool : 動畫控制器 的 設定布林值
        //代表是否需要跳躍
        anim.SetBool ( "Jump" , JumpBool );
        //代表是否踩在地上
        anim.SetBool ( "Ground" , IsGround );
    }

    void SetAnimSpeed ( float speed )
    {
        anim.speed = speed;
    }

    public void Hurt ()
    {
        //代表玩家受到傷害
        anim.SetTrigger ( "Hurt" );
        HurtBool = true;                    //目前已經受傷
        //停止移動
        body.velocity = new Vector2 ( 0 , body.velocity.y );
        HPCtl.self.LoseHP();           //遺失生命                                    
    }
}
