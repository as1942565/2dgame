﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TouchThing : MonoBehaviour
{
    private void OnCollisionEnter2D ( Collision2D collision )
    {
        if ( collision.gameObject.tag == "Scene" )      //如果撞到場景物件
        {
            Touch ( collision );
        }
    }

    private void OnTriggerEnter2D ( Collider2D collider )
    {
        if ( collider.tag == "Scene" )      //如果撞到場景物件
        {
            Trigger ( collider );
        }
    }
    
    void Touch ( Collision2D collision )
    {
        //如果碰到 的 遊戲物件 的 名字 等於 Block_Root 而且 碰到他的下方
        //撞到磚頭
        if ( collision.gameObject.name == "Block_Root" && collision.contacts[0].normal == -Vector2.up )
        {
            //傳遞已碰到
            if ( PlayerDataManager.self.data.Power )
            {
                ScoreCtl.self.AddScore ( 10 );      //分數得10分
            }  
            collision.gameObject.GetComponent<BlockCtl> ().Hit ();
        }

        //如果碰到 的 遊戲物件 的 名字 等於 "CoinBlock_Root" 而且 碰到他的下方
        //撞到錢磚
        if ( collision.gameObject.name == "CoinBlock_Root" && collision.contacts[0].normal == -Vector2.up )
        {
            //傳遞已碰到
            collision.gameObject.GetComponent<CoinBlockCtl> ().Hit ();
        }

        //如果碰到 的 遊戲物件 的 名字 等於 "Enemy_Root" 而且 碰到他的上方
        //撞到敵人
        if ( collision.gameObject.name == "Enemy_Root" && collision.contacts[0].normal == Vector2.up )
        {
            ScoreCtl.self.AddScore ( 100 );      //分數得100分
            //傳遞已碰到
            collision.gameObject.GetComponent<EnemyCtl> ().Hit ();
        }

        //如果碰到 的 遊戲物件 的 名字 等於 "Complete" 而且 碰到他的上方
        //撞到敵人
        if ( collision.gameObject.name == "Complete" )
        {
            CompleteCtl.self.Complete ();
        }
    }

    void Trigger ( Collider2D collider )
    {
        if ( collider.name == "Coin_Root" )
        {
            ScoreCtl.self.AddScore ( 50 );      //分數得50分
            collider.GetComponent<CoinCtl> ().Get ();
        }
    }
}
