﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerEvent : MonoBehaviour
{
    public void HurtEnd ()              //受傷結束
    {
        PlayerCtl.self.HurtBool = false;//目前沒有受傷
        //目前需要開始閃爍
        PlayerInvincibleTime.self.IsInvincible = true;
    }
}
