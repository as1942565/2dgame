﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerInvincibleTime : MonoBehaviour
{
    public bool IsInvincible;      //判斷玩家現在是否無敵
   
    public static PlayerInvincibleTime self;
    private void Awake ()
    {
        self = this;
    }

    private float FlickerCD = 0.1f; //玩家閃爍的CD
    private float MaxFlickerCD;     //實際閃爍的時間
    private float FlickerNum;       //閃爍次數
    
    private SpriteRenderer playerBody;

    private void Start ()
    {
        playerBody = transform.GetChild ( 0 ).GetComponent<SpriteRenderer> ();
    }

    private void Update ()
    {
        //在不是暫停的狀態
        if ( PauseCtl.self.PauseBool == false )
        {
            Flicker ();     //玩家閃爍
        }
        
    }

    void Flicker ()         //玩家閃爍
    {
        if ( Time.time >= MaxFlickerCD && IsInvincible )                //如果現在時間 超過 實際閃爍的時間
        {
            FlickerNum++;
            playerBody.enabled = !playerBody.enabled;
            MaxFlickerCD = Time.time + FlickerCD;       //更新冷卻時間
            if ( FlickerNum >= 20 )
            {
                FlickerNum = 0;
                playerBody.enabled = true;
                IsInvincible = false;
            }
        }

    }
}
