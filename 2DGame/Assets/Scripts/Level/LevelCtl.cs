﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class LevelCtl : MonoBehaviour
{
    public Text WorldText;
    public Text LifeText;

    private void Start ()
    {
        int level = PlayerDataManager.self.data.Level;
        WorldText.text = PlayerDataManager.self.data.LevelNum ( level );
        LifeText.text = "x " + PlayerDataManager.self.data.LifeNum.ToString();

        Invoke ( "GO" , 2f );
    }

    void GO ()
    {
        LoadingSceneCtl.self.ChangeScene ( "Main" , 0.5f );
    }
}
