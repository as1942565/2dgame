﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CompleteCtl : MonoBehaviour
{

    public static CompleteCtl self;         //建構式
    private void Awake ()
    {
        self = this;
    }

    /*private void Update ()
    {
        if ( ChangeSceneBool && Input.GetKeyDown ( KeyCode.Return ) )
        {
            LoadingSceneCtl.self.ChangeScene ( "Level" , 0.5f );
            ChangeSceneBool = false;
        }
    }*/

    //private bool ChangeSceneBool;

    public void Complete ()                 //過關
    {
        Debug.Log ( "Complete" );
        //讓玩家變回待機動畫
        PlayerCtl.self.transform.GetChild ( 0 ).GetComponent<Animator> ().SetBool ( "Run" , false );
        PauseCtl.self.Pause ( true );       //遊戲 (假) 暫停

        Invoke ( "ChangeScene" , 2 );       //2秒後才會開始淡出
    }
    void ChangeScene ()
    {
        //ChangeSceneBool = true;
        PlayerDataManager.self.Save ();                     //進行存檔
        PlayerDataManager.self.data.Level += 1;                         //關卡數加1
        LoadingSceneCtl.self.ChangeScene ( "Level" , 0.5f );//0.5秒後進訴關卡資訊場景
    }
}
