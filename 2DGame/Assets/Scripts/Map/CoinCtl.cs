﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CoinCtl : MonoBehaviour
{
    Animator anim;          //動畫控制器

    private void Start ()
    {
        anim = transform.GetChild ( 0 ).GetComponent<Animator> ();       //取得動畫控制器
    }

    private void Update ()
    {
        /*//如果現在是暫停的狀態
        if ( PauseCtl.self.PauseBool == true )
        {
            anim.speed = 0;                     //將敵人的動畫播放速度改為0
        }
        else                                    //否則
        {
            anim.speed = 1;                     //將敵人的動畫播放速度改為1
        }*/
        anim.speed = PauseCtl.self.PauseBool ? 0 : 1;
    }

    public void Get ()      //得到金幣
    {
        GetCoinCtl.self.AddCoin ( 1 );
        anim = transform.GetChild ( 0 ).GetComponent<Animator> ();       //取得動畫控制器
        anim.SetTrigger ( "Get" );                         //得到金幣
        GetComponent<BoxCollider2D> ().enabled = false;    //關閉碰撞框
        Destroy ( this.gameObject , 0.4f );                //0.4秒後刪除物件
 
    }
}
