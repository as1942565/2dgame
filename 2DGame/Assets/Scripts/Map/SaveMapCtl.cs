﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;

//儲存地圖用
public class SaveMapCtl : MonoBehaviour             //Scenes/Maker
{
    public Vector2 Pos;                             //地圖編輯器的位置(二維陣列要儲存的位置)
    public int ID;                                  //物件編號

    public static SaveMapCtl self;                  //建構式
    private void Awake ()
    {
        self = this;
    }
    public Transform Scene;                         //生產地圖物件的位置

    private int [,] Maps;                           //地圖物件編號(存檔用)
    private GameObject [,] Objs;                    //地圖物件(玩家觀看用)
    private int Height = 8;                         //地圖的高度
    private int Weight = 31;                        //地圖的寬度

    private const string SavePath = "Assets/Resources/Data/Scene/Level";

    private void Start ()
    {
        Maps = new int [Weight , Height];           //設定二維陣列的大小
        Objs = new GameObject [Weight , Height];    //設定二維陣列的大小
    }

    private void Update ()
    {
        if ( Input.GetKeyDown ( KeyCode.Space ) )   //如果按下空白鍵
        {
            BuildMap ();                            //建立地圖物件
        }
        if ( Input.GetKeyDown ( KeyCode.Return ) )  //如果按下Enter鍵
        {
            SaveMap ();                             //儲存地圖
        }
    }

    void BuildMap ()                                //建立地圖物件
    {
        Maps [(int)Pos.x , (int)Pos.y] = ID;        //紀錄地圖編號

        //如果該位置已經有物件
        if ( Objs [(int)Pos.x , (int)Pos.y] != null )
            //就將它刪除
            Destroy ( Objs [(int)Pos.x , (int)Pos.y] );
        //建立地圖物件
        GameObject obj = Instantiate ( MakerCtl.self.Target , Scene ) as GameObject;
        obj.transform.localPosition = Pos;          //更改地圖物件的位置
        obj.transform.GetChild ( 0 ).GetComponent<SpriteRenderer> ().sortingOrder = 2; 
        obj.SetActive ( true );                     //顯示地圖物件
        Objs [(int)Pos.x , (int)Pos.y] = obj;       //紀錄地圖物件
    }

    void SaveMap ()                                 //儲存地圖
    {
        string map = "";
        for ( int y = 0; y < Height; y++ )
        {
            for ( int x = 0; x < Weight; x++ )
            {
                map += Maps [x , y].ToString () + ',';
            }
            map = map.Substring ( 0 , map.Length - 1 );
            map += '\n';
        }
        //map = map.Substring ( 0 , map.Length - 1 );

        File.WriteAllText ( SavePath + "1" + ".txt" , map.Substring ( 0 , map.Length - 1 ) );
        Debug.Log ( "SaveMap！" );
    }



}
