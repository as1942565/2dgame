﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BackgroundCtl : MonoBehaviour
{
    private int Num = 10;
    private float Distance = 10f;

    //const : 鎖住自定義的值
    private const string SkyPrefabPath = "Prefabs/Maps/Sky1";       //用途 : 背景的Prefab的路徑

    private void Start ()           //只執行一次
    {
        //for迴圈 : 可以重複執行很多次
        //for ( 起始值; 條件式; 遞增值 )
        for ( int i = 2; i < Num; i++ )
        {
            //Instantiate : 生產遊戲物件
            //Resources.Load : 在Assets的Resources資料夾取得資料
            
            //生產一個從Resources資料夾取得的Prefab的遊戲物件 並且將它變成 掛載此腳本的子物件
            GameObject sky = ( GameObject )Instantiate ( Resources.Load ( SkyPrefabPath ) , transform ) as GameObject;
            sky.transform.localPosition = new Vector3 ( Distance * i , 0 , 0 );

        }
    }
}
