﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnCoin : MonoBehaviour
{
    private const string CoinPrefabPath = "Prefabs/Scenes/Coin_Root";

    public void Spawn ()
    {
        //生產金幣
        GameObject coin = Instantiate ( Resources.Load ( CoinPrefabPath ) ) as GameObject;
        //調整位置
        coin.transform.localPosition = transform.position + new Vector3 ( 0 , 1 , 0 );
        //播放獲得金幣動畫 以及後續處理
        coin.GetComponent<CoinCtl> ().Get ();
    }
}
