﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BlockCtl : MonoBehaviour
{
    Animator anim;          //動畫控制器

    public void Hit ()      //撞到磚頭
    {
        anim = transform.GetChild ( 0 ).GetComponent<Animator> ();       //取得動畫控制器
        anim.SetTrigger ( "Hit" );              //撞到
        anim.SetBool ( "Power" , PlayerDataManager.self.data.Power );                //判斷玩家是否有力量來決定磚頭移動或破裂

        if ( PlayerDataManager.self.data.Power )
        {
            GetComponent<BoxCollider2D> ().enabled = false;    //關閉碰撞框
            Destroy ( this.gameObject , 0.4f );                //0.4秒後刪除物件
        }  
    }
}
