﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MakerMoveCtl : MonoBehaviour
{
    private float HoldCD = 0.3f;                        //按住時 初次等待的時間
    private float ComboCD = 0.05f;                      //兩次以上的等待時間
    private float MaxHoldCD = 0f;                       //實劑的等待時間
    private float MoveDistance = 1f;                    //編輯器移動距離
    private bool KeyUp;                                 //偵測是否有按住上鍵
    private bool KeyDown;                               //偵測是否有按住下鍵
    private bool KeyLeft;                               //偵測是否有按住左鍵
    private bool KeyRight;                              //偵測是否有按住右鍵

    private Vector2 XRange = new Vector2 ( 0 , 30 );
    private Vector2 YRange = new Vector2 ( 0 , 7 );
    private Vector3 MakerPos;                           //紀錄編輯器會移動到的位置

    private void Update ()
    {
        KeyUp = Input.GetKey ( KeyCode.UpArrow );       //是否有按住上鍵
        KeyDown = Input.GetKey ( KeyCode.DownArrow );   //是否有按住下鍵
        KeyLeft = Input.GetKey ( KeyCode.LeftArrow );   //是否有按住左鍵
        KeyRight = Input.GetKey ( KeyCode.RightArrow ); //是否有按住右鍵

        if ( KeyUp || KeyDown )                         //如果有按住上鍵 或 下鍵
        {
            UpAndDown ();
        }
        if ( KeyLeft || KeyRight )                      //如果有按住左鍵 或 右鍵
        {
            LeftAndRight ();
        }
        //取得X軸範圍內的位置
        MakerPos.x = Mathf.Clamp ( MakerPos.x , XRange.x , XRange.y ); 
        //取得Y軸範圍內的位置
        MakerPos.y = Mathf.Clamp ( MakerPos.y , YRange.x , YRange.y );
        //更新編輯器的位置
        transform.localPosition = MakerPos;
        SaveMapCtl.self.Pos = MakerPos;
        MakerCamCtl.self.CamMove ( transform.position.x );

    }

    void UpAndDown ()
    {
        if ( Input.GetKeyDown ( KeyCode.UpArrow ) )     //如果按了上鍵
        {
            MoveDistance = Mathf.Abs ( MoveDistance );  //移動距離就改為正的
            MaxHoldCD = Time.time + HoldCD;             //增加初次的等待時間
            //移動Y軸
            MakerPos += new Vector3 ( 0 , MoveDistance , 0 ); 
        }
        else if ( Input.GetKeyDown ( KeyCode.DownArrow ) )//如果按了下鍵
        {
            MoveDistance = -Mathf.Abs ( MoveDistance ); //移動距離就改為負的
            MaxHoldCD = Time.time + HoldCD;             //增加初次的等待時間
            //移動Y軸
            MakerPos += new Vector3 ( 0 , MoveDistance , 0 );
        }

        if ( Time.time > MaxHoldCD )                    //如果超過初次的等待時間
        {
            MaxHoldCD = Time.time + ComboCD;            //就增加二次以上的冷卻時間
            //移動Y軸
            MakerPos += new Vector3 ( 0 , MoveDistance , 0 ); 
        }
    }

    void LeftAndRight ()                                
    {
        if ( Input.GetKeyDown ( KeyCode.RightArrow ) )  //如果按了右鍵
        {
            MoveDistance = Mathf.Abs ( MoveDistance );  //移動距離就改為正的
            MaxHoldCD = Time.time + HoldCD;             //增加初次的等待時間
            //移動X軸
            MakerPos += new Vector3 ( MoveDistance , 0 , 0 );
        }
        else if ( Input.GetKeyDown ( KeyCode.LeftArrow ) )//如果按了左鍵
        {
            MoveDistance = -Mathf.Abs ( MoveDistance ); //移動距離就改為負的
            MaxHoldCD = Time.time + HoldCD;             //增加初次的等待時間
            //移動X軸
            MakerPos += new Vector3 ( MoveDistance , 0 , 0 );
        }

        if ( Time.time > MaxHoldCD )                    //如果超過初次的等待時間
        {
            MaxHoldCD = Time.time + ComboCD;            //就增加二次以上的冷卻時間
            //移動X軸
            MakerPos += new Vector3 ( MoveDistance , 0 , 0 );
        }
    }
}
