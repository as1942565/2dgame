﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MakerCtl : MonoBehaviour
{
    public GameObject Target;                           //現在顯示的圖示

    public static MakerCtl self;                        //建構式
    private void Awake ()
    {
        self = this;
    }

    private int ObjQuantity;                            //物件數量
    private int ObjID = 0;                              //目前選擇的物件編號

    private void Start ()
    {
        ObjQuantity = transform.childCount;             //取得該遊戲物件的子物件數量 (不會抓到子物件的子物件)
        Target = transform.GetChild ( ObjID ).gameObject;
        for ( int i = 0; i < ObjQuantity; i++ )
        {
            //根據子物件編號來顯示物件
            transform.GetChild ( i ).gameObject.SetActive ( i == ObjID );
        }
    }

    private void Update ()
    {
        if ( Input.GetKeyDown ( KeyCode.X ) )           //如果按了X鍵
        {
            Change ( 1 );                               //切換編號(加1)
        }
        else if ( Input.GetKeyDown ( KeyCode.Z ) )      //如果按了Z鍵
        {
            Change ( -1 );                              //切換編號(減1)
        }       
    }

    void Change ( int num )                             //切換編號
    {
        ObjID += num;                                   //增加 / 減少編號
        if ( ObjID >= ObjQuantity )                     //如果物件編號 大於等於 物件數量
            ObjID = 0;                                  //讓物件編號等於0
        if ( ObjID < 0 )                                //如果物件編號 小於 0
            ObjID = ObjQuantity - 1;                    //讓物件編號等於物件數量-1

        SaveMapCtl.self.ID = ObjID + 1;                 //傳送物件編號

        Target = transform.GetChild ( ObjID ).gameObject;   //紀錄現在顯示的物件 (給閃爍用的)
        for ( int i = 0; i < ObjQuantity; i++ )
        {
            //根據子物件編號來顯示物件
            transform.GetChild ( i ).gameObject.SetActive ( i == ObjID );
        }
    }
}
