﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MakerFlicker : MonoBehaviour
{
    private bool FlickerBool = false;                       //是否要顯示物件

    private void Start ()
    {
        Invoke ( "Flicker" , 0.5f );                        //每秒執行一次
    }

    void Flicker ()
    {
        MakerCtl.self.Target.SetActive ( FlickerBool );     //顯示 / 隱藏物件
        FlickerBool = !FlickerBool;                         //顯示 / 隱藏
        Invoke ( "Flicker" , 0.5f );                        //每秒執行一次
    }
}
