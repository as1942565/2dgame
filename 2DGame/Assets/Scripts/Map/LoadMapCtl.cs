﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LoadMapCtl : MonoBehaviour
{
    private float Distance = 1f;

    private const string DataPath = "Data/Scene/Level";
    private const string SceneObjPath = "Prefabs/Scenes/";

    private string [] SceneObjName = new string [] { "" , "Block_Root" , "Coin_Root" , "CoinBlock_Root" , "Enemy_Root" , "Player_Root" };

    private List<GameObject> SceneObjs = new List<GameObject> ();

    private void Start ()
    {
        Load ();
    }

    void Load ()
    {
        string data = Resources.Load<TextAsset> ( DataPath + "1" ).text;
        string [] RowY = data.Split ( '\n' );
        for ( int y = 0; y < RowY.Length; y++ )
        {
            string [] RowX = RowY [y].Split ( ',' );
            for ( int x = 0; x < RowX.Length; x++ )
            {
                if ( int.Parse ( RowX [x] ) != 0 )
                {
                    GameObject obj = Instantiate ( Resources.Load ( SceneObjPath + SceneObjName [int.Parse ( RowX [x] )] ) , transform ) as GameObject;
                    obj.transform.localPosition = new Vector3 ( Distance * x , Distance * y , 0 );
                    obj.name = SceneObjName [int.Parse ( RowX [x] )];
                    SceneObjs.Add ( obj );
                    if ( int.Parse ( RowX [x] ) == 5 )
                    {
                        obj.transform.parent = null;
                        obj.transform.localPosition += new Vector3 ( 0 , 1 , 0 );
                        MainCamCtl.self.Player = obj.transform;
                    }
                }
                
                
            }
        }
       
    }
}
