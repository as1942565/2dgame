﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CoinBlockCtl : MonoBehaviour
{
	Animator anim;                  //動畫控制器

    private int CoinNum = 10;       //磚頭裡有10個金幣

    public void Hit ()              //撞到磚頭
    {
        if ( CoinNum > 0 )
        {
            ScoreCtl.self.AddScore ( 50 );      //分數得50分
        }

        anim = transform.GetChild ( 0 ).GetComponent<Animator> ();       //取得動畫控制器
        CoinNum--;
        anim.SetTrigger ( "Hit" );              //撞到
        anim.SetBool ( "GetCoin" , CoinNum > 0 );                //判斷金幣磚頭是否還有錢
    }
}
