﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyCtl : MonoBehaviour
{
    private float speed = -8f;
    private float TurnCD = 0.3f;        //每0.3秒判段是否要轉向
    private float MaxTurnCD = 0.0f;     //要轉向的真實時間

    Rigidbody2D body;                   //物理效果
    Vector2 MyselfPos;                  //紀錄自己的座標
    Animator anim;                      //動畫控制器

    private void Start ()
    {
        anim = transform.GetChild ( 0 ).GetComponent<Animator> ();       //取得動畫控制器
        body = GetComponent<Rigidbody2D> ();    //取得物理效果
        MyselfPos = transform.localPosition;    //紀錄自己的座標
        MaxTurnCD = Time.time + TurnCD;
    }

    private void FixedUpdate ()
    {
        //如果現在是暫停的狀態
        if ( PauseCtl.self.PauseBool == true )
        {
            anim.speed = 0;                     //將敵人的動畫播放速度改為0
            body.velocity = Vector2.zero;       //將敵人的移動瞬間改為0
            return;                             //回傳
        }
        else                                    //否則
        {
            anim.speed = 1;                     //將敵人的動畫播放速度改為1
        }

        body.velocity = new Vector2 ( speed , body.velocity.y );

        //Time.time : 遊戲運行的真實時間
        if ( Time.time >= MaxTurnCD )
        {
            //Debug.Log (Vector2.Distance ( MyselfPos, transform.localPosition ));
            //Vector2.Distance : 兩者之間連一條線 這條線的距離
            if ( Vector2.Distance ( MyselfPos, transform.localPosition ) < 0.5f )
                speed *= -1;
            MyselfPos = transform.localPosition;    //重新紀錄自己的座標
            MaxTurnCD = Time.time + TurnCD;
        }

        //Mathf.Sign 只要小於0的數值 都會回傳 -1 否則回傳1 ( 會幫你判斷這個數值是不是負數 )
        float Turn = Mathf.Sign ( speed );
        if ( speed != 0 )
           transform.localScale = new Vector3 ( -Turn , 1 , 1 );       //我們將 ( Turn , 1 , 1 ) 設定給 座標 的 相對大小
    }

    public void Hit ()                          //被玩家睬到
    {
        anim.SetTrigger ( "Dead" );              //被睬到
        Destroy ( this.gameObject , 0.4f );
        GetComponent<Rigidbody2D> ().constraints = RigidbodyConstraints2D.FreezePositionY;
        speed = 0f;
    }

    private void OnCollisionEnter2D ( Collision2D collision )
    {
        //如果撞到左邊跟右邊的牆壁
        if ( collision.contacts[0].normal == Vector2.right || collision.contacts[0].normal == -Vector2.right )
        {
            speed *= -1;
            MyselfPos = transform.localPosition;    //重新紀錄自己的座標
            MaxTurnCD = Time.time + TurnCD;
        }
        //如果碰到 的 遊戲物件 的 名字 等於 Player_Root 而且 不到碰到他的上方 而且 玩家目前還沒受傷 而且 玩家不是無敵狀態
        //怪物攻擊玩家
        if ( collision.gameObject.name == "Player_Root" && collision.contacts[0].normal != -Vector2.up && !PlayerCtl.self.HurtBool && !PlayerInvincibleTime.self.IsInvincible )
        {
            //傳遞已碰到
            collision.gameObject.GetComponent<PlayerCtl> ().Hurt ();
        }
    }

    /*private void OnCollisionStay2D ( Collision2D collision )
    {
        //如果碰到 的 遊戲物件 的 名字 等於 Player_Root 而且 不到碰到他的上方 而且 玩家目前還沒受傷 而且 玩家不是無敵狀態
        //怪物攻擊玩家
        if ( collision.gameObject.name == "Player_Root" )
        Debug.Log ( collision.contacts[0].normal );
        if ( collision.gameObject.name == "Player_Root" && collision.contacts[0].normal != -Vector2.up && !PlayerCtl.self.HurtBool && !PlayerInvincibleTime.self.IsInvincible )
        {
            Debug.Log ("Hit");
            //傳遞已碰到
            collision.gameObject.GetComponent<PlayerCtl> ().Hurt ();
        }
    }*/
}
